# igwn_alert_supervisor

`igwn_alert_supervisor` is designed to run alongside an instance of `GraceDB` and perform actions directly in the database. By bypassing `django`'s REST API, overall latency of superevent creation and management is reduced.