#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) Alexander Pace (2022)
#
# This file is part of lvalert-overseer
#
# igwn_alert_supervisor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn_alert_supervisor. If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup, find_packages

version = "0.0.1"
AUTHOR = 'Alexander Pace'
AUTHOR_EMAIL = 'alexander.pace@ligo.org'
LICENSE = 'GPLv3'

description = "IGWN Alert Supervisor"
long_description = "The IGWN Alert Supervisor interacts with " \
    " GraceDB's database in response to igwn_alerts"

setup(
    name="igwn-alert-supervisor",
    version=version,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    description=description,
    long_description=long_description,
    url=None,
    license=LICENSE,
    namespace_packages=['igwn_supervisor'],
    packages=find_packages(),
    scripts=['bin/igwn_alert_supervisor'],
    install_requires=['psycopg2',
                      'igwn-alert>=0.1.3'
                      ],
)
