FROM debian:buster
LABEL name="igwn-alert-supervisor" \
      maintainer="alexander.pace@ligo.org" \
      date="20220126"

# Copy source directory
RUN mkdir src
ADD . src/

# Update system packages and install pip and support packages:
RUN apt-get update && \
    apt -y upgrade && \
    apt-get install --install-recommends --assume-yes \
        python3-pip \
        libpq-dev && \
    apt-get clean

# Upgrade pip:
RUN pip3 install --upgrade pip

# Install to the system:
RUN cd src && \
    pip3 install .

# Copy over the supervisord control file to the correct location:
COPY docker/supervisord-igwnsupervisor.conf /etc/supervisor/conf.d/igwnsupervisor.conf

# Make an 'overseer' user to run via supervisor:
RUN groupadd igwnsupervisor
RUN useradd -M -u 50001 -g igwnsupervisor -s /bin/false igwnsupervisor

# Copy over the entrypoint script. This pulls in secrets
# from docker secrets.
COPY docker/entrypoint /usr/local/bin/entrypoint
RUN chmod 0755 /usr/local/bin/entrypoint

# Install supervisor:
RUN pip3 install supervisor

# Close out
ENTRYPOINT [ "/usr/local/bin/entrypoint" ]
CMD ["/usr/local/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]
