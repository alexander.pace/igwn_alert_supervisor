# Copyright (C) Alexander Pace (2022)
#
# This file is part of igwn_alert_supervisor
#
# igwn_alert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn_alert.  If not, see <http://www.gnu.org/licenses/>.

# -------------------------------------------------------------------
#   Collect input options into dictionaries
# -------------------------------------------------------------------

def assemble_options(options):

    DATABASE = {
        'database': options.db_name,
        'user':     options.db_username,
        'password': options.db_password,
        'host':     options.db_host,
        'port':     options.db_port,
    }

    ALERTS = {
        'server':   options.kafka_server,
        'username': options.igwn_username,
        'password': options.igwn_password,
        'group':    options.igwn_group,
    }

    return DATABASE, ALERTS
