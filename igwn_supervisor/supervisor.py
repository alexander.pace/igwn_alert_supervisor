# Copyright (C) Alexander Pace (2022)
#
# This file is part of igwn_alert_supervisor
#
# igwn_alert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn_alert.  If not, see <http://www.gnu.org/licenses/>.

from igwn_supervisor.api import add_log_to_event


class alert_processor(object):

    def __init__(self, connection=None):

        print("Alert processor initiated")
        if connection:
            self.conn = connection
        else:
            raise ValueError("no connection defined!")

    def process(self, topic=None, payload=None):

        print("recieved an alert from topic %s" % (topic))

        if ('superevent' in topic):
            print('superevent alert')
        else:
            # Do something if there is a 'new' event alert:
            if (payload['alert_type'] == 'new'):
                add_log_to_event(self.conn,
                                 uid=payload['uid'])

            else:
                print('This is not a new event, {}'.format(
                    payload['uid']))
