# Copyright (C) Alexander Pace (2022)
#
# This file is part of igwn_alert_supervisor
#
# igwn_alert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn_alert_supervisor. If not, see <http://www.gnu.org/licenses/>.

# Note: 1) ensure that autocommit=True on the connection object
#       2) ensure that datetime objects are in UTC before committing.

from datetime import datetime


def add_log_to_event(db_conn, uid=None, msg=None):

    ''' Here is a sample INSERT to create a log message for an event:

    INSERT INTO "events_eventlog" ("created", "issuer_id", "filename",
    "file_version", "comment", "N", "event_id") VALUES
    ('2022-01-10T19:26:08.406122+00:00'::timestamptz, 55, '', NULL, '<p>This is
    a test message.&nbsp;</p>', 29, 403622) RETURNING "events_eventlog"."id"

    '''

    issuer_id = 55    # This is alexander.pace@LIGO.org in dev1
    comment = "Log message inserted by igwn_alert_supervisor."

    cmd = 'INSERT INTO "events_eventlog" ' \
          '("created", "issuer_id", "filename", '\
          '"file_version", "comment", "N", "event_id") VALUES '\
          "(%(timenow)s, %(issuer_id)s, '', NULL, %(comment)s, "\
          '(SELECT MAX("N") from events_eventlog ' \
          'WHERE event_id=%(event_id)s)+1, %(event_id)s) '\
          'RETURNING "events_eventlog"."id";'

    with db_conn.cursor() as curs:
        curs.execute(cmd, {'timenow': datetime.utcnow(),
                           'issuer_id': issuer_id,
                           'comment': comment,
                           'event_id': uid[1:]})
